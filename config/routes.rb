Rails.application.routes.draw do

  mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      token_validations:  'overrides/token_validations'
  }

  root to: 'root#index'

  namespace :api do
    namespace  :v1 do
      get '' => 'users#index'
      resources :items
      resources :categories
      resources :deliveries
      resources :plans
      resources :plan_types
      resources :orders
    end
  end

  get '*path', to: 'root#index'
end
