require 'rails_helper'
require 'faker'

RSpec.describe Api::V1::CategoriesController, type: :controller do
  describe "GET index returns valid data for unregistered user: " do
    before do
      parent = create(:category)

      get :index
      @category_sample = JSON.parse(response.body)['categories'][0]
    end

    it "returns status 200" do
      expect(response).to have_http_status(200)
    end

    it "response includes valid keys" do
      expect(@category_sample.keys).to match_array(["id", "name", "description", "ancestry"])
    end

    it "response not includes invalid keys" do
      expect(@category_sample.keys).not_to include("created_at", "updated_at")
    end
  end

  describe "GET show returns valid data" do
    before do
      @category = create(:category)
      get :show, params: { id: @category.id }
      @category_sample = JSON.parse(response.body)
    end

    it "returns status 200" do
      expect(response).to have_http_status(200)
    end

    it "response includes valid keys" do
      expect(@category_sample.keys).to match_array(["id", "name", "description", "ancestry"])
    end

    it "response not includes invalid keys" do
      expect(@category_sample.keys).not_to include("created_at", "updated_at")
    end
  end
end
