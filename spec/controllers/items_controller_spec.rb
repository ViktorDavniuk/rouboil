require 'rails_helper'
require 'faker'

RSpec.describe Api::V1::ItemsController, type: :controller do
  describe "GET index returns valid data for unregistered user: " do
    before do
      create(:item)
      get :index
      @item_sample = JSON.parse(response.body)['items'][0]
    end

    it "returns status 200" do
      expect(response).to have_http_status(200)
    end

    it "response includes valid keys" do
      expect(@item_sample.keys).to match_array(["id", "name", "short_description", "description", "pictures"])
    end

    it "response not includes invalid keys" do
      expect(@item_sample.keys).not_to include("published", "deleted", "created_at", "updated_at")
    end

    it "does not show deleted items" do
      @item = create(:item, deleted: true)
      get :index
      expect(JSON.parse(response.body)['items']).not_to include(@item.as_json)
    end

    it "shows only published items" do
      @item = create(:item, published: false)
      get :index
      expect(JSON.parse(response.body)['items']).not_to include(@item.as_json)
    end
  end

  describe "GET index filter items depends on params: " do
    before do
      @category1 = create(:category_with_items, items_count: 1)
      @category2 = create(:category_with_items, items_count: 1)
      @cat1_item_id = @category1.items.first.id
      @cat2_item_id = @category2.items.first.id
      @indep_item_id = create(:item).id
    end
    it 'returns all items, when category is not set ' do
      get :index
      @items = JSON.parse(response.body)['items'].map()
      expect(@items).to include(@cat1_item, @cat2_item, @indep_item)
    end
    it 'returns only one category items, when one category is set ' do
      get :index, params: { filters: { categories: [@category1.id] } }
      @items = JSON.parse(response.body)['items']
      expect(@items).to include(@cat1_item)
      expect(@items).not_to include(@cat2_item, @indep_item)
    end
    it 'returns only several category items, when several categories are set ' do
      get :index, params: { filters: { categories: [@category1.id, @category2.id] } }
      @items = JSON.parse(response.body)['items']
      expect(@items).to include(@cat1_item, @cat2_item)
      expect(@items).not_to include(@indep_item)
    end
  end

  describe "GET show returns valid data" do
    before do
      @item = create(:item)
      get :show, params: { id: @item.id }
      @item_sample = JSON.parse(response.body)
    end

    it "returns status 200" do
      expect(response).to have_http_status(200)
    end

    it "response includes valid keys" do
      expect(@item_sample.keys).to match_array(["id", "name", "short_description", "description", "pictures"])
    end

    it "response not includes invalid keys" do
      expect(@item_sample.keys).not_to include("published", "deleted", "created_at", "updated_at")
    end

    it "request to deleted item raises error" do
      @item = create(:item, deleted: true)
      expect { get :show, params: { id: @item.id } }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it "request to unpublished item raises error" do
      @item = create(:item, published: false)
      expect { get :show, params: { id: @item.id } }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
