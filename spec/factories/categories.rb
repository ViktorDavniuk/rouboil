require 'faker'

FactoryBot.define do
  factory :category do
    name { Faker::Restaurant.type }
    description { Faker::Lorem.paragraph(8, true, 10) }

    trait :with_subcategory do
      after(:create) do |category|
        category.children << FactoryBot.create(:category_with_items)
      end
    end

    factory :category_with_items do
      transient do
        items_count { 5 }
      end

      after(:create) do |category, eval|
        eval.items_count.times do
          a = FactoryBot.create(:item)
          category.items << a
        end
      end
    end
  end
end
