require 'faker'

FactoryBot.define do
  factory :plan do
    name { Faker::Commerce.product_name }
    status { "created" }
    begin_at { Date.today }
  end
end