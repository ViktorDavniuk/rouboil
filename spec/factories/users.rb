require 'faker'

FactoryBot.define do
  factory :user, aliases: [:author] do |u|
    name { Faker::Name.name }
    sequence(:email) { |n| "user#{n}@user.com" }
    password { "password" }
    avatar { URI.parse(Faker::LoremFlickr.image("150x150")) }

    trait :admin do
      name { "Admin" }
      email { "admin@admin.com" }
      after(:create) {|user| user.add_role(:admin)}
    end
  end
end
