require 'faker'

FactoryBot.define do
  factory :plan_type do
    name { Faker::Commerce.product_name }
    short_description { Faker::Lorem.paragraph }
    description { Faker::Lorem.paragraph(8, true, 10) }
    orders_count { Random.rand(1...30) }
    max_duration { Random.rand(1...30) }
    deleted { false }
    published { true }

    trait :unpublished do
      published { false }
    end

    trait :deleted do
      deleted { true }
    end
  end
end