require 'faker'

FactoryBot.define do
  factory :picture do
    image { URI.parse(Faker::LoremFlickr.image("270x270")) }
  end
end
