require 'faker'

FactoryBot.define do
  factory :item do
    name { Faker::Commerce.product_name }
    short_description { Faker::Lorem.paragraph }
    description { Faker::Lorem.paragraph(8, true, 10) }
    deleted { false }
    published { true }

    trait :unpublished do
      published { false }
    end

    trait :deleted do
      deleted { true }
    end

    after(:create) do |item|
      item.pictures << FactoryBot.create(:picture, item: item)
    end
  end
end