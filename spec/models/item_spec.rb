require 'rails_helper'

RSpec.describe Item, type: :model do
  it { should have_many(:pictures).dependent(:destroy) }
  it { should have_many(:categories).through(:item_categories) }
end
