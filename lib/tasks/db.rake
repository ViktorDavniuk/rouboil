namespace :db do
  desc "drop bases, create bases, migrate bases, seed bases"
  task recreate: :environment do
    Rake::Task["db:drop"].invoke
    Rake::Task["db:create"].invoke
    Rake::Task["db:migrate"].invoke
    Rake::Task["db:seed_fu"].invoke
  end
end
