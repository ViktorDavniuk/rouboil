class CreatePictures < ActiveRecord::Migration[5.1]
  def change
    create_table :pictures do |t|
      t.integer :item_id, index: true

      t.timestamps
    end
  end
end
