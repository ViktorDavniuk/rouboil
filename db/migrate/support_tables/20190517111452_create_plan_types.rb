class CreatePlanTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :plan_types do |t|
      t.string :name
      t.string :short_description
      t.text :description
      t.integer :orders_count
      t.integer :max_duration
      t.boolean :deleted, default: false
      t.boolean :published, default: false

      t.timestamps
    end
  end
end
