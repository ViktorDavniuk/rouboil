class CreateObjectStatuses < ActiveRecord::Migration[5.1]
  def change
    create_table :object_statuses do |t|
      t.string :status
      t.string :object_type

      t.timestamps
    end
  end
end
