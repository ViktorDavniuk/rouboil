class CreateUserOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :user_orders do |t|
      t.integer :customer_id, index: true
      t.integer :order_id, index: true

      t.timestamps
    end
  end
end
