class CreateUserDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table :user_deliveries do |t|
      t.integer :customer_id, index: true
      t.integer :delivery_id, index: true

      t.timestamps
    end
  end
end
