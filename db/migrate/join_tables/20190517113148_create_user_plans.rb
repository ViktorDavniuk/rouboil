class CreateUserPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :user_plans do |t|
      t.integer :customer_id, index: true
      t.integer :plan_id, index: true

      t.timestamps
    end
  end
end
