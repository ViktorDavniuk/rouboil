class CreatePlans < ActiveRecord::Migration[5.1]
  def change
    create_table :plans do |t|
      t.string :name
      t.integer :plan_type_id, index: true
      t.string :status
      t.datetime :begin_at
      t.datetime :end_at
      t.boolean :orders_generated, default: false
      t.boolean :done, default: false
      t.boolean :paid, default: false
      t.text :comment
      t.timestamps
    end
  end
end
