class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :plan_id
      t.boolean :complete, default: false
      t.boolean :done, default: false
      t.boolean :delivery_created, default: false
      t.string :status

      t.timestamps
    end
  end
end
