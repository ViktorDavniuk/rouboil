class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :manufactor, index: true
      t.string :name
      t.string :short_description
      t.text :description
      t.boolean :published, default: false
      t.boolean :deleted, default: false

      t.timestamps
    end
  end
end
