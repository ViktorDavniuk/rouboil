class CreateDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table :deliveries do |t|
      t.datetime :planned
      t.datetime :courier_confirmed
      t.datetime :customer_confirmed
      t.boolean :agreed, default: false
      t.boolean :done, default: false

      t.timestamps
    end
  end
end
