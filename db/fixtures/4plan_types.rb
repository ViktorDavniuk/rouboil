require 'factory_bot_rails'

FactoryBot.create(:plan_type, {
    name: "Пробный",
    orders_count: 1,
    max_duration: 5
})
FactoryBot.create(:plan_type, {
    name: "Неделя",
    orders_count: 7,
    max_duration: 14
})
FactoryBot.create(:plan_type, {
    name: "Две недели",
    orders_count: 14,
    max_duration: 30
})
FactoryBot.create(:plan_type, {
    name: "Месяц",
    orders_count: 30,
    max_duration: 90
})
FactoryBot.create(:plan_type, {
    name: "Три месяца",
    orders_count: 90,
    max_duration: 180
})
FactoryBot.create(:plan_type, {
    name: "Полгода",
    orders_count: 180,
    max_duration: 270
})

