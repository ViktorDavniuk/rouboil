require 'factory_bot_rails'

5.times do
  cat = FactoryBot.create(:category)
  5.times do
    FactoryBot.create(:category_with_items, parent: cat)
  end
end
