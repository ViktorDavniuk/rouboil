class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.by_role(role, user)
    # Rolify returns array against ActiveRecord:Relation that's why this unoptimal workaround have been used
    # to return ActiveRecord:Relation by user role.
    self.where(id: self.with_role(role, user).map(&:id))
  end
end
