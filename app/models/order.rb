class Order < ApplicationRecord
  has_one :user_order
  has_one :customer, source: :user, through: :user_order
  belongs_to :plan
end
