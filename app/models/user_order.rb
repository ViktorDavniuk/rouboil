class UserOrder < ApplicationRecord
  belongs_to :user, foreign_key: "customer_id"
  belongs_to :order, dependent: :destroy
end
