class Plan < ApplicationRecord
  resourcify
  before_save :set_end_at_attribute
  after_save :create_orders

  has_one :user_plan
  has_one :customer, source: :user, through: :user_plan
  has_many :orders
  belongs_to :plan_type

  private

  # set "end_at" date depends on duration of a plan and a "begin_at" date
  # after "end_date" customer will not be able to do any manipulations with the plan or orders
  # the plan will be "done" as well as orders
  def set_end_at_attribute
      if self.new_record?
        self.status = "created"
        self.begin_at = Time.now
        self.end_at = self.begin_at + (self.plan_type.max_duration).days
      end
  end

  # creates orders for the plan, depends on plan_type
  def create_orders
    unless self.orders_generated
      #CreateOrdersByPlanJob.perform_now(self)
      self.transaction do
        self.plan_type.orders_count.times do
          self.orders.create(customer: self.customer)
        end
        self.update_attributes(orders_generated: true)
      end
    end
  end
end
