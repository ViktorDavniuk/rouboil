class Delivery < ApplicationRecord
  has_one :user_delivery
  has_one :customer, source: :user, through: :user_delivery
end
