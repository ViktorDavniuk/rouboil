class Item < ApplicationRecord
  # rolify resource
  resourcify

  has_many :pictures, dependent: :destroy
  has_many :item_categories
  has_many :categories, through: :item_categories

  default_scope { includes(:pictures) }

  # filterrific gem filters setup
  scope :categories, -> (categories) { joins(:item_categories).where("category_id IN (?)", categories) }

  filterrific(
      available_filters: [
          :categories
      ]
  )

end
