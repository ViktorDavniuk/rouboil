class Picture < ApplicationRecord
  has_attached_file :image,
                    styles: { thumb: "100x100>", medium: "270x270" },
                    default_url: "/images/missing.png",
                    path: ":rails_root/public/:attachment/:id/:style/:basename.jpg",
                    url: "localhost:3000/public/:attachment/:id/:style/:basename.jpg"
  validates_attachment_content_type :image,
                                    content_type: /\Aimage\/.*\z/,
                                    size: { in: 0..2000.kilobytes }

  belongs_to :item

  def image_url
    self.image.url
  end
end
