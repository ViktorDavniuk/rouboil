class UserPlan < ApplicationRecord
  belongs_to :user, foreign_key: "customer_id"
  belongs_to :plan, dependent: :destroy
end
