class UserDelivery < ApplicationRecord
  belongs_to :user, foreign_key: "customer_id"
  belongs_to :delivery, dependent: :destroy
end
