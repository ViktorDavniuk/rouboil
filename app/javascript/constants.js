import $ from 'jquery';
import * as Auth from 'j-toker';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

import reducer from './reducers/root-reducer';
import { DEVELOPMENT } from './features';

$.ajaxSetup({
    headers: {
        contentType: 'application/json',
        'X-Key-Inflection' : 'camel',
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    },
    beforeSend: function(xhr, settings) {
        $.auth.appendAuthHeaders(xhr, settings);
        return true;
    }
});

const ROOT_URL = DEVELOPMENT ? 'http://localhost:3000' : 'https://api.example.com';
const API_URL = ROOT_URL + '/api/v1'

$.auth.configure({
    apiUrl: ROOT_URL,
    passwordResetSuccessUrl: () => {
        return `${ROOT_URL}/reset_password`;
    }
});

export const to = (path) => {
    return (API_URL + path)
}

export const store = createStore(
    reducer,
    composeWithDevTools(
        applyMiddleware(
            thunkMiddleware
        )
    )
);
