import ItemsApi from '../api/items-api';

export const REQUEST_ITEMS = 'REQUEST_ITEMS';
export const RECEIVE_ITEMS = 'RECEIVE_ITEMS';
export const ADD_ITEMS = 'ADD_ITEMS';

export function requestItemsAction() {
    return {
        type: REQUEST_ITEMS
    };
}

export function receiveItemsAction(data, page) {
    return {
        type: RECEIVE_ITEMS,
        data: data,
        currentPage: page
    };
}

export function fetchItems(page, filters={}) {
    return function(dispatch) {
        ItemsApi.getItems(page, filters)
            .then((data) => {
                dispatch(receiveItemsAction(data, page));
            })
            .catch(() => {
                dispatch(receiveItemsAction({}));
            });
    };
}
