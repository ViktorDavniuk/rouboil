import PlanApi from '../api/plans-api';

export const REQUEST_PLANS = 'REQUEST_PLANS';
export const RECEIVE_PLANS = 'RECEIVE_PLANS';
export const PLAN_CREATING = 'PLAN_CREATING';
export const PLAN_CREATED  = 'PLAN_CREATED';
export const PLAN_DELETING = 'PLAN_DELETING';
export const PLAN_DELETED  = 'PLAN_DELETED';

export function requestPlansAction() {
    return {
        type: REQUEST_PLANS
    };
}

export function receivePlansAction(data) {
    return {
        type: RECEIVE_PLANS,
        data: data
    };
}

export function planCreatingAction() {
    return {
        type: PLAN_CREATING
    };
}

export function planCreatedAction(data) {
    return {
        type: PLAN_CREATED,
        data: data
    };
}

export function planDeletingAction() {
    return {
        type: PLAN_DELETING
    };
}

export function planDeletedAction() {
    return {
        type: PLAN_DELETED
    };
}

export function fetchPlans(user) {
    return function(dispatch) {
        dispatch(requestPlansAction());
        PlanApi.getPlans(user)
            .then((data) => {
                dispatch(receivePlansAction(data));
            })
            .catch(() => {
                dispatch(receivePlansAction({}));
            });
    };
}

export function createPlan(plan) {
    return function(dispatch) {
        dispatch(planCreatingAction());
        PlanApi.postPlan({ plan: plan })
            .then((data) => {
                dispatch(planCreatedAction(data))
            })
            .catch(() => {
                dispatch(planCreatedAction({}));
            });
    }
}

export function deletePlan(id) {
    return function(dispatch) {
        dispatch(planDeletingAction());
        console.log(id);
        PlanApi.deletePlan(id)
            .then(() => {
                fetchPlans()
                dispatch(planDeletedAction())
            });
    }
}