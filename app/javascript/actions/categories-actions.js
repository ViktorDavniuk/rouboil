import CategoriesApi from '../api/categories-api';

export const REQUEST_CATEGORIES = 'REQUEST_CATEGORIES';
export const RECEIVE_CATEGORIES = 'RECEIVE_CATEGORIES';
export const SELECT_CATEGORY = 'SELECT_CATEGORY';

export function requestCategoriesAction() {
    return {
        type: REQUEST_CATEGORIES
    };
}

export function selectCategoryAction(category) {
    return {
        type: SELECT_CATEGORY,
        data: {
            category: category.id,
            level: category.level
        }
    };
}

export function receiveCategoriesAction(data) {
    return {
        type: RECEIVE_CATEGORIES,
        data: data
    };
}

export function fetchCategories() {
    return function(dispatch) {
        CategoriesApi.getCategories()
            .then((data) => {
                dispatch(receiveCategoriesAction(data));
            })
            .catch(() => {
                dispatch(receiveCategoriesAction({}));
            });
    };
}

export function selectCategory(category) {
    return function(dispatch) {
        dispatch(selectCategoryAction(category))
    }
}
