import PlanTypesApi from '../api/plan-types-api';

export const REQUEST_PLAN_TYPES = 'REQUEST_PLAN_TYPES';
export const RECEIVE_PLAN_TYPES = 'RECEIVE_PLAN_TYPES';

export function requestPlanTypesAction() {
    return {
        type: REQUEST_PLAN_TYPES
    };
}

export function receivePlanTypesAction(data) {
    return {
        type: RECEIVE_PLAN_TYPES,
        data: data
    };
}

export function fetchPlanTypes() {
    return function(dispatch) {
        dispatch(requestPlanTypesAction());
        PlanTypesApi.getPlanTypes()
            .then((data) => {
                dispatch(receivePlanTypesAction(data));
            })
            .catch(() => {
                dispatch(receivePlanTypesAction({}));
            });
    };
}