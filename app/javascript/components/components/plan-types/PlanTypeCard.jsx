import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
    card: {
        maxWidth: 270,
        margin: "4px",
    },
    contentContainer: {
        margin: "4px",
        display: "flex",
        flexDirection: "column"
    },
    image: {
        height: "100px",
        width: "100px"
    },
    grow: {
        flexGrow: 1
    }
};

class PlanTypeCard extends Component {
    render() {
        return (
            <Card className={ this.props.classes.card }>
                <CardHeader
                    title={this.props.planType.name}
                />
                <div className={this.props.classes.contentContainer}>
                    <CardMedia
                        className={this.props.classes.image}
                        component={"img"}
                        src="http://s3.eu-central-1.amazonaws.com/routest/media/sf_exch/rouboil/public/images/127/original/270.jpg?1558344967"
                    />
                    <CardContent className={this.props.classes.grow}>
                        <Typography component="p">{this.props.planType.shortDescription}
                        </Typography>
                    </CardContent>
                </div>
                <CardActions disableActionSpacing>
                    <Button onClick={this.props.handleSubscribe}>Подписаться</Button>
                </CardActions>
            </Card>
        );
    }
}

export default withStyles(styles)(PlanTypeCard);