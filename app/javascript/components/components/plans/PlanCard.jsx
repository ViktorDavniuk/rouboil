import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import OrderCard from "../../components/orders/OrderCard";

const styles = {
    card: {
        display: "flex",
        flexDirection: "column"
    },
    ordersContainer: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap"
    },
    titleContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%'
    },
    titleActionsContainer: {
        display: 'flex',
        justifyContent: 'flex-end'
    }
};

class PlanCard extends Component {
    handleDelete() {
        this.props.handleDelete(this.props.plan.id)
    }
    render() {
        return (
                    <ExpansionPanel>
                        <ExpansionPanelSummary
                            expandIcon={ <ExpandMoreIcon /> }
                        >
                          <div className={ this.props.classes.titleContainer }>
                              <div>
                                  { this.props.plan.name }
                               </div>
                              <div className={ this.props.classes.titleActionsContainer }>
                                  <IconButton
                                      aria-label="Delete"
                                      size={'small'}
                                      onClick={ this.handleDelete}>
                                      <DeleteIcon />
                                  </IconButton>
                              </div>
                          </div>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails
                            className={this.props.classes.ordersContainer}
                        >
                            {
                                this.props.plan.orders.map((order, i) => {
                                    return (
                                        <OrderCard order={order} key={i} />
                                    )
                                })
                            }
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
        );
    }
}

export default withStyles(styles)(PlanCard);