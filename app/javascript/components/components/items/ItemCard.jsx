import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    card: {
        maxWidth: 270,
        margin: "8px",
        display: "flex",
        flexDirection: "column"
    },
    image: {
        height: "270px",
        width: "270px"
    },
    grow: {
        flexGrow: 1
    }
};

class ItemCard extends React.Component {
    render() {
        return (
            <Card className={ this.props.classes.card }>
                <CardHeader
                    action={
                        <IconButton>
                            <MoreVertIcon />
                        </IconButton>
                    }
                    title={this.props.item.name}
                    subheader="September 14, 2016"
                />
                <CardMedia
                    className={this.props.classes.image}
                    component={"img"}
                    src={this.props.item.pictures[0].imageUrl}
                    title="Paella dish"
                />
                <CardContent className={this.props.classes.grow}>
                    <Typography component="p">{this.props.item.shortDescription}
                    </Typography>
                </CardContent>
                <CardActions disableActionSpacing>
                    <IconButton aria-label="Add to favorites">
                        <FavoriteIcon />
                    </IconButton>
                    <IconButton aria-label="Share">
                        <ShareIcon />
                    </IconButton>
                    <IconButton
                        aria-label="Show more"
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
            </Card>
        );
    }
}

export default withStyles(styles)(ItemCard);