import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

const styles = {
    card: {
        margin: 10
    }
};

class OrderCard extends Component {
    render() {
        return (
            <Card className={ this.props.classes.card }>
                <CardHeader
                    title={this.props.order.name}
                    subheader="Действует с: 15.12.12 до: 16.12.12"
                />
                <CardContent>
                </CardContent>
            </Card>
        );
    }
}

export default withStyles(styles)(OrderCard);