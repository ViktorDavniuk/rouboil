import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core";

const styles = {
    menuButton: {
        color: "white"
    }
}

class MenuButton extends Component {
    render() {
        return (
                <Button
                    className={this.props.classes.menuButton}
                    component={Link}
                    label={this.props.label}
                    to={this.props.to}>
                        {this.props.label}
                </Button>
        );
    }
}

export default withStyles(styles)(MenuButton);