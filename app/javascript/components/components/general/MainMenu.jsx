import React from 'react';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuButton from './MenuButton';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import PropTypes from 'prop-types';
import AuthApi from "../../../api/auth-api";
import { fetchCurrentUser } from "../../../actions/user-actions";

const styles = {
    brand: {
        marginRight: "18px"
    },
    menuLinks: {
        display: "flex",
        flexGrow: 1
    }
}

class MainMenu extends React.Component {
    state = {
        menuOpened: false,
        anchorEl: null
    }

    componentWillMount() {
        this.props.dispatch(fetchCurrentUser());
    }

    handleSignOut() {
        AuthApi.signOut().then(() => {
            window.location.href = '/';
        });
    }

    handleClose() {
        this.setState({anchorEl: null, menuOpened: false })
    }

    handleMenu(event) {
        this.setState({ anchorEl: event.currentTarget, menuOpened: true })
    }

    render() {
        return (
            <AppBar position="static">
                <Toolbar>
                    <IconButton color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <div className={this.props.classes.menuLinks}>
                        <Typography variant="h6" color="inherit" className={this.props.classes.brand}>
                            Photos
                        </Typography>
                        <MenuButton
                            label="Main"
                            to="/"/>
                        <MenuButton
                            label="Items"
                            to="/items"/>
                        <MenuButton
                            label="Plans"
                            to="/plans"/>
                        <MenuButton
                            label="Deliveries"
                            to="/items"/>
                    </div>
                    { _.isEmpty(this.props.currentUser) ?
                        <MenuButton
                            label="Войти"
                            to="/sign_in" /> :
                        <div>
                            <div onClick={this.handleMenu.bind(this)}>
                                <IconButton
                                    aria-owns={open ? 'menu-appbar' : undefined}
                                    aria-haspopup="true"
                                    color="inherit"
                                >
                                    <AccountCircle />
                                </IconButton>

                            </div>
                            <Menu
                                anchorEl={this.state.anchorEl}
                                id="menu-appbar"
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={this.state.menuOpened}
                                onClose={this.handleClose.bind(this)}
                            >
                                <MenuItem onClick={this.handleClose.bind(this)}>Profile</MenuItem>
                                <MenuItem onClick={this.handleSignOut}>Выход</MenuItem>
                            </Menu>
                        </div>}
                </Toolbar>
            </AppBar>
        );
    };
}

function mapStateToProps(state) {
    return {
        isReady: state.users.isReady,
        currentUser: state.users.currentUser
    }
}

export default withStyles(styles)(connect(mapStateToProps)(MainMenu));