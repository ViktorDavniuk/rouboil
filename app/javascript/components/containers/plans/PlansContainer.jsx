import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import MainMenu from "../../components/general/MainMenu";
import PlanTypes from './PlanTypesContainer';
import PlansList from './PlansListContainer';
import _ from 'lodash';

const styles = {};

class Plans extends Component {
    render() {
        return (
            <div className="content">
                <MainMenu />
                { !_.isEmpty(this.props.currentUser) && <PlansList /> }
                <PlanTypes />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentUser: state.users.currentUser
    };
}

export default withStyles(styles)(connect(mapStateToProps)(Plans));