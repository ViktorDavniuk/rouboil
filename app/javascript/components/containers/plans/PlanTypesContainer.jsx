import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { fetchPlanTypes } from '../../../actions/plan-types-actions';
import { createPlan } from '../../../actions/plans-actions';
import PlanTypeCard from '../../components/plan-types/PlanTypeCard';
import PlanCreateForm from '../../forms/plans/PlanCreateForm';


const styles = {
    planTypesContainer: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between"
    }
};

class PlansTypes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showForm: false,
            planTypeId: null
        };
    }

    handleClose() {
        this.setState({ showForm: false })
    }

    handleCreate(plan) {
        this.props.dispatch(
            createPlan(
                Object.assign( plan, { planTypeId: this.state.planTypeId })
            )
        )
        this.handleClose()
    }

    handleShowCreatePlanForm(planTypeId) {
        this.setState({
            showForm: true,
            planTypeId: planTypeId
        })
    }

    componentWillMount() {
        this.props.dispatch(fetchPlanTypes());
    }


    render() {
        if (!this.props.isReady) return null;
        return (
            <div className={this.props.classes.planTypesContainer}>
                <PlanCreateForm
                    showDialog={this.state.showForm}
                    planType={this.state.planTypeId}
                    handleClose={this.handleClose.bind(this)}
                    handleCreate={this.handleCreate.bind(this)}
                />
                {
                    this.props.planTypes.map((planType, i) => {
                        return(<PlanTypeCard key={i}
                                             planType={planType}
                                             handleSubscribe={this.handleShowCreatePlanForm.bind(this, planType.id)}
                        />)
                    })
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isReady: state.planTypes.isReady,
        planTypes: state.planTypes.list
    };
}

export default withStyles(styles)(connect(mapStateToProps)(PlansTypes));