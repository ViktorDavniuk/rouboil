import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import PlanCard from "../../components/plans/PlanCard";
import { fetchPlans, deletePlan } from "../../../actions/plans-actions";

const styles = {
    cardsContainer: {
        display: "flex",
        flexDirection: "column",
        width: '100%'
    }
};

class PlansList extends Component {
    componentWillMount() {
        this.props.dispatch(fetchPlans());
    }
    handleDelete(planId) {
        this.props.dispatch(deletePlan(planId));
    }
    render() {
        if (!this.props.isReady) return null;
        return (
            <div className={this.props.classes.cardsContainer}>
                {
                    this.props.plans.map( (plan, i) => {
                        return (
                            <PlanCard
                                key={i}
                                plan={plan}
                                handleDelete={() => this.handleDelete.bind(this)}
                            /> )
                    })
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isReady: state.plans.isReady,
        plans: state.plans.list
    };
}

export default withStyles(styles)(connect(mapStateToProps)(PlansList));