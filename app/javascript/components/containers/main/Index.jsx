import React from 'react';

import MainMenu from '../../components/general/MainMenu';

class Index extends React.Component {

    render() {
        return (
            <div className="content">
                <MainMenu />
            </div>
        );
    }
}
export default Index;
