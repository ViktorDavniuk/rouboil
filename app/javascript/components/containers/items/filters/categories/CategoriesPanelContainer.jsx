import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import _ from 'lodash'

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';

const styles = {
    categoriesRow: {
        display: "flex",
        flexDirection: "row"
    }
};

class CategoriesPanelContainer extends Component {

    renderCategoriesTreeFrom(id) {
        return (
            this.getChildrenById(id).map((category, i) => {
                return(
                    <List key={i}>
                        <ListItem component={"div"} key={category.id}>
                            <ListItemText key={category.id +"_text"} primary={ category.name } />
                        </ListItem>
                        <Collapse in={true} timeout="auto" unmountOnExit>
                            { this.renderCategoriesTreeFrom(category.id) }
                        </Collapse>
                    </List>
                );
            })
        );
    }

    getChildrenById(id) {
        return (_.filter(this.props.categories, { ancestry: id }));
    }

    render() {
        return (
            <div className={this.props.categoriesRow}>
                {
                    this.renderCategoriesTreeFrom(null)
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        categories: state.categories.list
    };
}

export default withStyles(styles)(connect(mapStateToProps)(CategoriesPanelContainer));