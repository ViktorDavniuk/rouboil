import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import CategoriesPanel from "./categories/CategoriesPanelContainer"
import { fetchCategories } from '../../../../actions/categories-actions';


const styles = ({
    root: {
        width: '100%'
    },
    icon: {
        verticalAlign: 'bottom',
        height: 20,
        width: 20,
    },
    details: {
        alignItems: 'center',
    }

});

class FilterPanelContainer extends Component {
    componentWillMount() {
        this.fetchCategories();
    }

    fetchCategories() {
          this.props.dispatch(fetchCategories());
    }

    render() {
        return (
            <div className={this.props.classes.root}>
                <ExpansionPanel className={this.props.classes.panel}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                        <Grid container>
                            <Grid item xs={12} sm={6}>
                               <Typography>Категории:</Typography>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <Typography>Фильтры:</Typography>
                            </Grid>
                        </Grid>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={this.props.classes.details}>
                        <Grid container>
                            <Grid item xs={12} sm={6}>
                                <CategoriesPanel />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <div>
                                    <Typography>Фильтры не выбраны</Typography>
                                </div>
                            </Grid>
                        </Grid>
                    </ExpansionPanelDetails>
                    <Divider/>
                    <ExpansionPanelActions>
                        <Button size="small" color="primary">
                            Применить
                        </Button>
                    </ExpansionPanelActions>
                </ExpansionPanel>
            </div>
        );
    }
}

function mapStateToProps(state) {
     return {
         categories: state.categories.list
     };
}

export default withStyles(styles)(connect(mapStateToProps)(FilterPanelContainer));