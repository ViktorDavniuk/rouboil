import React from 'react';
import { connect } from 'react-redux';
import { Waypoint } from 'react-waypoint';
import ItemCard from '../../components/items/ItemCard';
import MainMenu from "../../components/general/MainMenu";
import FilterPanel from "./filters/FilterPanelContainer"

import { fetchItems } from '../../../actions/items-actions';
import { withStyles } from "@material-ui/core";

const styles = {
    cardsContainer: {
        display: "flex",
        flexWrap: "wrap"
    }
}

class Items extends React.Component {

    combineFilters() {
        return ({
            categories: []
        })
    }

    _fetchItems(page) {
        this.props.dispatch(fetchItems(page, this.combineFilters.bind(this)));
    }

    fetchItemsNextPage() {
        this._fetchItems(this.props.currentPage+1)
    }

    componentWillMount() {
        this._fetchItems(1);
    }

    render() {
        if (!this.props.isReady) return null;
        return (
            <div className="content">
                <MainMenu />
                <FilterPanel />
                    <div className={this.props.classes.cardsContainer}>
                        {
                            this.props.items.map( (item, i) => {
                                return ( <ItemCard key={i} item={item} /> )
                            })
                        }
                    </div>
                {!this.props.lastPage && <Waypoint onEnter={this.fetchItemsNextPage.bind(this)} />}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isReady: state.items.isReady,
        items: state.items.list,
        currentPage: state.items.currentPage,
        lastPage: state.items.lastPage,
        categories: state.categories
    };
}

export default withStyles(styles)(connect(mapStateToProps)(Items));
