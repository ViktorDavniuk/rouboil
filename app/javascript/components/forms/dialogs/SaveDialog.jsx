import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Formik } from 'formik';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = {};

class PlanCreateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showDialog: false,
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleAction = this.handleAction.bind(this);
    }

    handleClickOpen() {
        this.setState({ showDialog: true });
    }

    handleClose() {
        this.setState({ showDialog: false });
    }

    handleAction() {
        handleClose();
        this.props.action();
    }
    render() {
        return (
            <div>
                <Dialog open={this.state.showDialog} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">{ this.props.title }</DialogTitle>
                    <DialogContent>
                        { this.props.children }
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Отмена
                        </Button>
                        <Button onClick={this.handleAction} color="primary">
                            Сохранить
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(PlanCreateForm);