import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = {
    form: {
        display: 'flex',
        flexDirection: 'column',
        margin: 10
    }
};

class PlanCreateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            comment: ''
        };

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeComment = this.onChangeComment.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }

    onChangeName(event){
        this.setState({
            name: event.target.value
        });
    }

    onChangeComment(event) {
        this.setState(
            {
                comment: event.target.value
            });
    }

    handleCreate() {
        this.props.handleCreate(this.state)
    }

    render() {
        return (
            <div>
                <Dialog open={this.props.showDialog} onClose={this.props.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Подписаться на план</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            После подписки на план будут сформированы заказы.
                        </DialogContentText>
                            <form
                                className={this.props.classes.form}>
                                <TextField
                                    type="text"
                                    label={"Название"}
                                    onChange={this.onChangeName}
                                    value={this.state.name}
                                    name="name"
                                />
                                <TextField
                                    type="text"
                                    label={"Комментарий"}
                                    onChange={this.onChangeComment}
                                    value={this.state.comment}
                                    name="comment"
                                />
                            </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.handleClose} color="primary">
                            Отмена
                        </Button>
                        <Button onClick={ this.handleCreate } color="primary">
                            Добавить
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

export default withStyles(styles)(PlanCreateForm);