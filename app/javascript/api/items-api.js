import $ from 'jquery';
import { to } from '../constants'

//$.ajax({
//    type: "POST",
//    url: to("/items"),
//    data: "name=John&location=Boston",
//    success: function(msg){
//        alert( "Прибыли данные: " + msg );
//    }
//});

let ItemsApi = {
    getItems(page, filters) {
        return $.ajax({
            type: "GET",
            url: to("/items"),
            data: {
                page: page,
                filters: JSON.stringify(filters)
            }
        });
    }
}

export default ItemsApi;
