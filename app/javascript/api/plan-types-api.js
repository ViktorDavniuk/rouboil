import $ from 'jquery';
import { to } from '../constants'

//$.ajax({
//    type: "POST",
//    url: to("/items"),
//    data: "name=John&location=Boston",
//    success: function(msg){
//        alert( "Прибыли данные: " + msg );
//    }
//});

let PlanTypesApi = {
    getPlanTypes() {
        return $.ajax({
            type: "GET",
            url: to("/plan_types")
        });
    }
}

export default PlanTypesApi;
