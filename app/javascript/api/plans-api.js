import $ from 'jquery';
import { to } from '../constants'

const path = "/plans"

let PlansApi = {
    getPlans() {
        return $.ajax({
            type: "GET",
            url: to(path)
        });
    },
    getPlan(id) {
        return $.ajax({
            type: "GET",
            url: to(path) + `/${id}`
        });
    },
    postPlan(plan) {
        return $.ajax({
            type: "POST",
            url: to(path),
            data: plan
        })
    },
    deletePlan(id) {
        return $.ajax({
            type: "DELETE",
            url: to(path) + `/${id}`
        })
    }
}

export default PlansApi;
