import $ from 'jquery';
import { to } from '../constants'

//$.ajax({
//    type: "POST",
//    url: to("/items"),
//    data: "name=John&location=Boston",
//    success: function(msg){
//        alert( "Прибыли данные: " + msg );
//    }
//});

let CategoriesApi = {
    getCategories() {
        return $.ajax({
            type: "GET",
            url: to("/categories")
        });
    }
}

export default CategoriesApi;
