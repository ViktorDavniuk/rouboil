import * as PlansActions from '../actions/plans-actions';

const initialState = {
    isReady: false,
    loading: false,
    list: []
};

export function plansReducer(state = initialState, action) {
    switch (action.type) {
        case PlansActions.REQUEST_PLANS:
            return Object.assign({}, state, {
                isReady: false
            });
        case PlansActions.RECEIVE_PLANS:
            return Object.assign({}, state, {
                isReady: true,
                list: action.data.plans
            });
        case PlansActions.PLAN_CREATING:
            return Object.assign({}, state, {
                loading: true
            });
        case PlansActions.PLAN_CREATED:
            const result = [...state.list, action.data];
            return Object.assign({}, state, {
                loading: false,
                list: result
            });
        case PlansActions.PLAN_DELETING:
            return Object.assign({}, state, {
                loading: true
            });
        case PlansActions.PLAN_DELETED:
            return Object.assign({}, state, {
                loading: false
            });
        default:
            return state;
    }
}
