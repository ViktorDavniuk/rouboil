import * as CategoriesActions from '../actions/categories-actions';

const initialState = {
    isReady: false,
    list: [],
    currentLevel: null,
    currentCategory: null
};

export function categoriesReducer(state = initialState, action) {
    switch (action.type) {
        case CategoriesActions.REQUEST_CATEGORIES:
            return Object.assign({}, state, {
                isReady: false
            });
        case CategoriesActions.RECEIVE_CATEGORIES:
            return Object.assign({}, state, {
                isReady: true,
                list: action.data.categories
            });
        case CategoriesActions.SELECT_CATEGORY:
            return Object.assign({}, state, {
                currentLevel: data.level,
                currentCategory: data.category
            })
        default:
            return state;
    }
}
