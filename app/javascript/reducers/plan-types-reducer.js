import * as PlanTypesActions from '../actions/plan-types-actions';

const initialState = {
    isReady: false,
    list: []
};

export function planTypesReducer(state = initialState, action) {
    switch (action.type) {
        case PlanTypesActions.REQUEST_PLAN_TYPES:
            return Object.assign({}, state, {
                isReady: false
            });
        case PlanTypesActions.RECEIVE_PLAN_TYPES:
            return Object.assign({}, state, {
                isReady: true,
                list: action.data.planTypes
            });
        default:
            return state;
    }
}
