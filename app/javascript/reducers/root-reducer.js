import { combineReducers } from 'redux';

import { userReducer } from './user-reducer';
import { itemsReducer } from './items-reducer';
import { categoriesReducer } from './categories-reducer';
import { plansReducer } from './plans-reducer';
import { planTypesReducer } from './plan-types-reducer'

const reducer = combineReducers({
    users: userReducer,
    items: itemsReducer,
    categories: categoriesReducer,
    plans: plansReducer,
    planTypes: planTypesReducer
});



export default reducer;
