import * as ItemsActions from '../actions/items-actions';
import _ from 'lodash'

const initialState = {
    isReady: false,
    list: [],
    currentPage: 1,
    lastPage: false
};

export function itemsReducer(state = initialState, action) {
    switch (action.type) {
        case ItemsActions.REQUEST_ITEMS:
            return Object.assign({}, state, {
                isReady: false
            });
        case ItemsActions.RECEIVE_ITEMS:
            const result = _.concat(state.list, action.data.items);
            return Object.assign({}, state, {
                isReady: true,
                list: result,
                currentPage: action.currentPage,
                lastPage: action.data.lastPage
            });
        default:
            return state;
    }
}
