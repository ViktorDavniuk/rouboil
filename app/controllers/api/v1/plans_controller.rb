class Api::V1::PlansController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_plan_class, only: [:index, :create]
  before_action :init_plan, only: [:show, :update, :destroy]

  def index
    @plans = policy_scope(Plan)
                 .joins(:orders, :plan_type).uniq
    render json: {
        plans: @plans.as_json(plan_json_structure)
    }, status: 200
  end

  def show
    render_plan
  end

  def create
    @plan = current_user.plans.new(plan_params)
    if @plan.save
      render_plan(201)
    else
      render_errors(@plan.errors)
    end
  end

  def update
    if @plan.update(plan_params)
      render_plan
    else
      render_errors(@plan.errors)
    end
  end

  def destroy
    @plan.orders.each do |order|
      render_errors({error => "Could not delete plan with processed orders."}) if order.status != "created"
    end
    head 204 if @plan.destroy
  end

  private

  def render_plan(_status: 200)
    render json: @plan.as_json(plan_json_structure), status: _status
  end

  def render_errors(errors)
    render json: errors, status: 422
  end

  def init_plan
    @plan = policy_scope(Plan).where(id: params[:id])
                .joins(:orders, :plan_type)
    authorize @plan
  end

  def authorize_plan_class
    authorize Plan
  end

  def render_plan(_status=200)
    render json: @plan.as_json(plan_json_structure),
           status: _status
  end

  def plan_params
    params.require(:plan).permit(policy(Plan).permitted_attributes)
  end

  def plan_json_structure
    json_structure("plan")
  end
end