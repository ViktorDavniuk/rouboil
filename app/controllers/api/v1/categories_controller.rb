class Api::V1::CategoriesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :authorize_category_class, only: [:index, :create]
  before_action :init_category, only: [:show, :update, :destroy]

  def index
    @categories = policy_scope(Category)
    render json: { categories: @categories.as_json(category_json_structure) },
           status: 200
  end

  def show
    render_category
  end

  def create
    @category = Category.new(_params)
    if @category.save
      render_category(201)
    else
      render json: @category.errors, status: 422
    end
  end

  def update
    if @category.update(_params)
      render_category
    else
      render json: @category.errors, status: 422
    end
  end

  def destroy
    head 204 if @category.destroy
  end

  private

  def init_category
    @category = policy_scope(Category).find(params[:id])
    authorize @category
  end

  def authorize_category_class
    authorize Category
  end

  def render_category(_status=200)
    render json: @category.as_json(category_json_structure),
           status: _status
  end

  def category_json_structure
    json_structure("category")
  end

  def category_params
    params.require(:category).permit(policy(@category).permitted_attributes)
  end
end
