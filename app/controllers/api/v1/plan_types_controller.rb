class Api::V1::PlanTypesController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :authorize_plan_type_class, only: [:index, :create]
  before_action :init_plan_type, only: [:show, :update, :destroy]

  def index
    @plan_types = policy_scope(PlanType)
    render json: {
        plan_types: @plan_types.as_json(plan_type_json_structure)
    }, status: 200
  end

  def show
    render_plan_type
  end

  def create
    @plan_type = PlanType.new(_params)
    if @plan_type.save
      byebug
      render_plan_type(201)
    else
      render_errors(@plan_type.errors)
    end
  end

  def update
    if @plan_type.update(_params)
      render_plan_type
    else
      render_errors(@plan_type.errors)
    end
  end

  def destroy
    head 204 if @plan_type.destroy
  end

  private

  def render_plan_type(_status: 200)
    render json: @plan_type.as_json(item_json_structure), status: _status
  end

  def render_errors(errors)
    render json: errors, status: 422
  end

  def init_plan_type
    @plan_type = policy_scope(PlanType).find(params[:id])
    authorize @plan_type
  end

  def authorize_plan_type_class
    authorize PlanType
  end

  def render_plan_type(_status=200)
    render json: @plan_type.as_json(plan_type_json_structure),
           status: _status
  end

  def plan_type_params
    params.require(:plan_type).permit(policy(@plan_types).permitted_attributes)
  end

  def plan_type_json_structure
    json_structure("plan_type")
  end
end