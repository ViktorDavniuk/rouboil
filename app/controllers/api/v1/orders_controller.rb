class Api::V1::OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_order_class, only: [:index]
  before_action :init_order, only: [:show, :update, :destroy]

  def index
    @orders = policy_scope(Order)
    render json: {
        orders: @orders.as_json(order_json_structure)
    }, status: 200
  end

  def show
    render_order
  end

  def update
    if @order.update(order_params)
      render_order
    else
      render_errors(@order.errors)
    end
  end

  def destroy
    head 204 if @order.destroy
  end

  private

  def render_order(_status=200)
    render json: @order.as_json(order_json_structure), status: _status
  end

  def render_errors(errors)
    render json: errors, status: 422
  end

  def init_order
    @order = policy_scope(Order).find(params[:id])
    authorize @order
  end

  def authorize_order_class
    authorize Order
  end

  def order_json_structure
    json_structure("order")
  end

  def order_params
    params.require(:order).permit(policy(@order).permitted_attributes)
  end
end
