class Api::V1::ItemsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :authorize_item_class, only: [:index, :create]
  before_action :init_item, only: [:show, :update, :destroy]

  def index
    # fusion filterrific gem for filter results by query filters
    # and pundit gem policy scope for filtering by user rights
    @filterrific = initialize_filterrific(
        policy_scope(Item),
        params[:filters],
        default_filter_params: {}
    ) || return

    # "find" is filterrific gem method, that returns AR collection
    @items = @filterrific
                 .find
                 .includes(:pictures)
                 .order(created_at: :desc)
                 .paginate(:page => params[:page], per_page: 25)

    # lastPage shows is it last page, for infinite scrolling implementation
    render json: {
        lastPage: @items.total_pages.to_s == params[:page],
        items: @items.as_json(item_json_structure)
    }, status: 200
  end

  def show
    render_item
  end

  def create
    @item = Item.new(item_params)
    # TODO: refactor with transaction?
    if @item.save && current_user.add_role(:owner, @item)
      render_item(201)
    else
      render_errors(@item.errors)
    end
  end

  def update
    if @item.update(item_params)
      render_item
    else
      render_errors(@item.errors)
    end
  end

  def destroy
    # TODO: refactor with safe deletion
    head 204 if @item.destroy
  end

  private

  def render_item(_status=200)
    render json: @item.as_json(item_json_structure), status: _status
  end

  def render_errors(errors)
    render json: errors, status: 422
  end

  def init_item
    @item = policy_scope(Item).find(params[:id])
    authorize @item
  end

  def authorize_item_class
    authorize Item
  end

  def item_json_structure
    json_structure("item")
  end

  def item_params
    params.require(:item).permit(policy(@item).permitted_attributes)
  end
end
