class Api::V1::PicturesController < ApplicationController
  before_action :init_picture, only: [:show, :update, :destroy]

  def index
    @pictures = Picture.all
    render json: @pictures, status: 200
  end

  def show
    render json: @picture, status: 200
  end

  def create
    @picture = Picture.new(_params)
    if @picture.save
      render json: @picture, status: 201
    else
      render json: @picture.errors, status: 422
    end
  end

  def update
    if @picture.update(_params)
      render json: @picture, status: 200
    else
      render json: @picture.errors, status: 422
    end
  end

  def destroy
    head 204 if @picture.destroy
  end

  private

  def init_picture
    @picture = Picture.find(params[:id])
  end

  def picture_params
    params.require(:picture).permit()
  end
end
