class JsonStructure
  class << self
    # return json config structure for as_json serializer depends on
    # user_role
    # @param [String] resource -- model class name i. e. "item"
    # @param [String] user_role -- user role i. e. "admin"
    def get_structure(resource, user_role="visitor")
      self.send("#{user_role}_#{resource}_structure")
    end


    # USERS json structures
    def customer_user_structure
      {
          only: [:id, :email, :name],
          methods: [:avatar_url]
      }
    end

    # ITEMS json structures
    def visitor_item_structure
     {
          only: [:id, :name, :short_description, :description, :pictures],
          include: {
              pictures: visitor_picture_structure
          }
     }
    end

    def customer_item_structure
      visitor_item_structure
    end

    def admin_item_structure
      visitor_item_structure
    end

    # CATEGORIES json structures
    def visitor_category_structure
      {
          only: [:id, :name, :description, :ancestry]
      }
    end

    def customer_category_structure
      visitor_category_structure
    end

    def admin_category_structure
      visitor_category_structure
    end

    # ORDERS json structures
    def visitor_order_structure
      { only: [] }
    end

    def customer_order_structure
      {
          only: [:id, :complete, :done, :delivery_created]
      }
    end

    def admin_order_structure
      visitor_order_structure
    end

    # PLANS json structures
    def visitor_plan_structure
      { only: [] }
    end

    def customer_plan_structure
      {
          only: [:id, :name, :begin_at, :end_at, :orders_generated, :paid, :status],
          include: {
              plan_type: {
                  only: [:name, :short_description, :orders_count]
              },
              orders: customer_order_structure
          }
      }
    end

    def admin_plan_structure
      customer_plan_structure
    end

    # PLAN_TYPES json structures
    def visitor_plan_type_structure
      {
          only: [:id, :name, :short_description, :description, :orders_count, :max_duration]
      }
    end

    def customer_plan_type_structure
      visitor_plan_type_structure
    end

    def admin_plan_type_structure
      visitor_plan_type_structure
    end
    
    # PICTURES json structures
    def visitor_picture_structure
      {
          only: [:id],
          methods: :image_url
      }
    end

    def customer_picture_structure
      visitor_picture_structure
    end

    def admin_picture_structure
      visitor_picture_structure
    end

    # TODO: refactor with global errors handling
    def method_missing(m, *args, &block)
      {
          errors: {
              error: ["#{m} json structure is not exist."]
          }
      }
    end
  end
end