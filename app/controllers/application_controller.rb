class ApplicationController < ActionController::Base
  require "support/json_structure"

  include Pundit
  after_action :verify_authorized, unless: :devise_controller?
  after_action :verify_policy_scoped, only: [:index, :show], unless: :devise_controller?

  include DeviseTokenAuth::Concerns::SetUserByToken

  protect_from_forgery with: :exception

  # TODO: refactor
  before_action :configure_permitted_parameters, if: :devise_controller?
  respond_to :json

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :password_confirmation, :name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:email, :password, :password_confirmation, :name, :avatar])
  end

  # return json structure depends on current_user role and model name
  # will use in as_json serializer
  # @param [String] _model -- name of model, i. e. "item"
  def json_structure(_model)
    JsonStructure.get_structure(
        _model,
        current_user.present? ? current_user.global_role : "visitor"
    )
  end

  def render_success
    render :json => { 'status': 'ok' }
  end

  def render_error(error)
    render :json => { 'errors': { error: [error] } }, :status => 400
  end

  def render_errors(model)
    render :json => { 'errors': model.errors.messages }, :status => 400
  end

  def not_found
    render :json => {}, :status => 404
  end
end
