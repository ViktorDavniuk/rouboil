class PlanTypePolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.present? && (user.has_role?(:admin))
  end

  def new?
    create?
  end

  def update?
    user.present? && (user.has_role?(:admin))
  end

  def edit?
    update?
  end

  def destroy?
    user.present? && (user.has_role?(:admin))
  end

  def permitted_fields
    []
  end

  def permitted_attributes
    []
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      user.present? && user.has_role?(:admin) ? scope.all
          : scope.where(published: true, deleted: false)
    end
  end
end
