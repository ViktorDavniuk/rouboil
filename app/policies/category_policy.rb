class CategoryPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.present? && user.has_role?(:admin) ? true : false
  end

  def new?
    create?
  end

  def update?
    user.present? && user.has_role?(:admin) ? true : false
  end

  def edit?
    update?
  end

  def destroy?
    user.present? && user.has_role?(:admin) ? true : false
  end

  def permitted_attributes
    if user.present? && user.has_role?(:admin)
      [:name, :description]
    else
      []
    end
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end
end
