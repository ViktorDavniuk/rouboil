class ItemPolicy < ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    true
  end

  def create?
    user.present? ? true : false
  end

  def new?
    create?
  end

  def update?
    user.present? && (user.has_role?(:admin) || user.has_role(:owner, @record)) ? true : false
  end

  def edit?
    update?
  end

  def destroy?
    user.present? && (user.has_role?(:admin) || user.has_role(:owner, @record)) ? true : false
  end

  def permitted_attributes
    if user.present? && (user.has_role?(:admin) || user.has_role(:owner, @record))
      [
          :manufactor,
          :name,
          :short_description,
          :description,
          :published,
          :deleted,
          filters:
              [
                  categories:
                      [:category_id]
              ]
      ]
    else
      [
          filters:
              [
                  categories:
                      [:category_id]
              ]
      ]
    end
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      # TODO: refactor
      if user.present?
        user.has_role?(:admin) ? scope.all :
            scope.where(published: true, deleted: false).or(scope.by_role(:owner, user))
      else
        scope.where(published: true, deleted: false)
      end
    end
  end
end
