class CreateOrdersByPlanJob < ApplicationJob
  queue_as :default

  def perform(plan)
    Plan.transaction do
      plan.plan_type.orders_count.times do
       plan.orders.create(customer: plan.customer)
      end
      plan.update_attributes(orders_generated: true)
    end
  end
end
